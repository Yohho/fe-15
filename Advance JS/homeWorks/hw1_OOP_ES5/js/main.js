/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
    this.size = size;
    this.stuffing = stuffing;
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = 'маленький гамбургер';
Hamburger.SIZE_LARGE = 'большой гамбургер';
Hamburger.STUFFING_CHEESE = 'сыр';
Hamburger.STUFFING_SALAD = 'салат';
Hamburger.STUFFING_POTATO = 'картошечка';
Hamburger.TOPPING_MAYO = 'начинка майонез';
Hamburger.TOPPING_SPICE = 'начинка перец';

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    if(topping === Hamburger.TOPPING_MAYO) {
        this.mayoTopping = topping;
    }
    else if (topping === Hamburger.TOPPING_SPICE) {
        this.spiceTopping = topping;
    }
}

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    if(topping === Hamburger.TOPPING_MAYO) {
        delete this.mayoTopping;
    }
    else if (topping === Hamburger.TOPPING_SPICE) {
        delete this.spiceTopping;
    }
}

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    this.arrOfToppings = [Hamburger.STUFFING_CHEESE, Hamburger.STUFFING_SALAD, Hamburger.STUFFING_POTATO,
        Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE]
}

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    if (this.size === Hamburger.SIZE_SMALL) {
        console.log('Вы выбрали маленький гамбургер')
    }
    else if (this.size === Hamburger.SIZE_LARGE) {
        console.log('Вы выбрали большой гамбургер')
    }
}

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    console.log(this.stuffing + ',' + (this.spiceTopping = ' ') + this.mayoTopping)
}

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    let price = null;
    if (this.size === Hamburger.SIZE_LARGE) {
        price += 100;
        console.log(price + ' грн')
    }
    // Надо доделать!!!
    if (this.size === Hamburger.SIZE_SMALL && this.stuffing === Hamburger.STUFFING_CHEESE) {
        price += 123;
        console.log(price + ' грн')
    } else if (this.size === Hamburger.SIZE_SMALL) {
        price += 50;
        console.log(price + ' грн')
    }

}

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {

}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
// function HamburgerException (...) { ... }




const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log(hamburger);

hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log(hamburger);

hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log(hamburger);

hamburger.removeTopping(Hamburger.TOPPING_SPICE)
console.log(hamburger);

hamburger.getToppings()
console.log(hamburger)

hamburger.getSize()

hamburger.getStuffing()

hamburger.calculatePrice()

