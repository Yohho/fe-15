function Person (name, surname, age) {
    this.name = name;
    this.surname = surname;
    this.age = age;
}

Person.prototype.sayHello = function () {
    console.log(`Hello, my name is ${this.name} ${this.surname}, and im ${this.age} y.o.`)
}

const person1 = new Person('Vitya', 'Dishkant', 20);
const person2 = new Person('Kristina', 'Kirpach', 21);

person1.sayHello();
person2.sayHello();